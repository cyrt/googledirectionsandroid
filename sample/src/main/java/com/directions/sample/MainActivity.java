package com.directions.sample;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.GoogleParser;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routes;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements RoutingListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, OnMapReadyCallback {
    protected GoogleMap map;
    protected LatLng start = new LatLng(48.8583604,2.2941983);
    protected LatLng end= new LatLng(48.8588377,2.2770206);
    @BindView(R.id.start)
    AutoCompleteTextView starting;
    @BindView(R.id.destination)
    AutoCompleteTextView destination;
    @BindView(R.id.send)
    ImageView send;
    private static final String LOG_TAG = "MyActivity";
    protected GoogleApiClient mGoogleApiClient;
    private PlaceAutoCompleteAdapter mAdapter;
    private ProgressDialog progressDialog;
    private List<Polyline> polylines;
    private static final int[] COLORS = new int[]{R.color.primary_dark, R.color.primary, R.color.primary_light, R.color.accent, R.color.primary_dark_material_light};


    private static final LatLngBounds BOUNDS_JAMAICA = new LatLngBounds(new LatLng(-57.965341647205726, 144.9987719580531),
            new LatLng(72.77492067739843, -9.998857788741589));

    /**
     * This activity loads a map and then displays the route and pushpins on it.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        Reader reader = new StringReader("{\"geocoded_waypoints\":[{\"geocoder_status\":\"OK\",\"place_id\":\"ChIJhTCVO8HAyRIRaALnNAxFjts\",\"types\":[\"street_address\"]},{\"geocoder_status\":\"OK\",\"place_id\":\"ChIJ__fv_i-_yRIRI-KpdsRWy4w\",\"types\":[\"street_address\"]},{\"geocoder_status\":\"OK\",\"place_id\":\"ChIJdT3enHC8yRIROx34FRg4KZw\",\"types\":[\"route\"]},{\"geocoder_status\":\"OK\",\"place_id\":\"ChIJo4DW2TWjyRIRqN58MbBtxlQ\",\"types\":[\"street_address\"]}],\"routes\":[{\"bounds\":{\"northeast\":{\"lat\":43.3034554,\"lng\":5.567589},\"southwest\":{\"lat\":43.2805498,\"lng\":5.3627759}},\"copyrights\":\"Map data ©2016 Google\",\"legs\":[{\"distance\":{\"text\":\"9.3 km\",\"value\":9298},\"duration\":{\"text\":\"18 mins\",\"value\":1090},\"end_address\":\"38 Avenue Désiré Bianco, 13011 Marseille, France\",\"end_location\":{\"lat\":43.2878751,\"lng\":5.425175299999999},\"start_address\":\"68 Quai du Port, 13002 Marseille, France\",\"start_location\":{\"lat\":43.2960805,\"lng\":5.3698717},\"steps\":[{\"distance\":{\"text\":\"0.4 km\",\"value\":447},\"duration\":{\"text\":\"2 mins\",\"value\":110},\"end_location\":{\"lat\":43.2953315,\"lng\":5.3644531},\"html_instructions\":\"Head <b>west</b> on <b>Quai du Port</b> toward <b>Rue de la Prison</b>\",\"polyline\":{\"points\":\"ogggGuxw_@Dh@L|CJdC@ZDz@D^H|@`@tDHt@H|@B`@Hl@@J?J@??R?B?H?VTjB\"},\"start_location\":{\"lat\":43.2960805,\"lng\":5.3698717},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.3 km\",\"value\":278},\"duration\":{\"text\":\"1 min\",\"value\":35},\"end_location\":{\"lat\":43.2967528,\"lng\":5.3628285},\"html_instructions\":\"Continue onto <b>Avenue Vaudoyer</b>\",\"polyline\":{\"points\":\"ybggGyvv_@TjB@N?F?F?P?F?HAJANAJAHADABCHADGJEFCFGFIHEBCBGBGDIBIBm@Hg@B[C}AI\"},\"start_location\":{\"lat\":43.2953315,\"lng\":5.3644531},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.5 km\",\"value\":494},\"duration\":{\"text\":\"1 min\",\"value\":69},\"end_location\":{\"lat\":43.3010427,\"lng\":5.364200299999999},\"html_instructions\":\"At the roundabout, continue straight onto <b>Quai de la Tourette</b>\",\"maneuver\":\"roundabout-right\",\"polyline\":{\"points\":\"ukggGulv_@AA?AAA?AA??AA??AA?A??AA?A?A?A??@A?A??@A??@A??@A@qB_@MEqAYs@MWGw@Oo@OwEmASEiEcA\"},\"start_location\":{\"lat\":43.2967528,\"lng\":5.3628285},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.3 km\",\"value\":271},\"duration\":{\"text\":\"1 min\",\"value\":44},\"end_location\":{\"lat\":43.3034226,\"lng\":5.3649469},\"html_instructions\":\"Continue onto <b>Quai de la Joliette</b>\",\"polyline\":{\"points\":\"ofhgGguv_@{AYeB]cASaEaASG\"},\"start_location\":{\"lat\":43.3010427,\"lng\":5.364200299999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"35 m\",\"value\":35},\"duration\":{\"text\":\"1 min\",\"value\":14},\"end_location\":{\"lat\":43.303268,\"lng\":5.3647275},\"html_instructions\":\"Make a <b>U-turn</b> at <b>Boulevard des Dames</b>\",\"maneuver\":\"uturn-left\",\"polyline\":{\"points\":\"kuhgG}yv_@G`@d@H\"},\"start_location\":{\"lat\":43.3034226,\"lng\":5.3649469},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.7 km\",\"value\":699},\"duration\":{\"text\":\"1 min\",\"value\":62},\"end_location\":{\"lat\":43.2971409,\"lng\":5.3630833},\"html_instructions\":\"Take the ramp onto <b>Tunnel de la Joliette</b>\",\"polyline\":{\"points\":\"mthgGqxv_@HL@@p@RdAXVFRDRBhAFzAHf@BJCJCNETDh@Hh@Jf@Jl@Jl@JJBLBfCj@hCl@LBLBNDND|AXVFh@NXH\"},\"start_location\":{\"lat\":43.303268,\"lng\":5.3647275},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.7 km\",\"value\":669},\"duration\":{\"text\":\"1 min\",\"value\":53},\"end_location\":{\"lat\":43.2916932,\"lng\":5.3657428},\"html_instructions\":\"Continue onto <b>Tunnel du Vieux-Port</b>\",\"polyline\":{\"points\":\"cnggGgnv_@l@Rb@Lh@J^DZ?bAAl@KTG^KZKf@UTMHGLKJK~CsCjAiAtBgB^UVOTM\\\\Qj@UB?dBs@\"},\"start_location\":{\"lat\":43.2971409,\"lng\":5.3630833},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"2.5 km\",\"value\":2549},\"duration\":{\"text\":\"3 mins\",\"value\":176},\"end_location\":{\"lat\":43.2824891,\"lng\":5.393229799999999},\"html_instructions\":\"Keep <b>left</b> to continue on <b>Tunnel Prado-Carénage</b><div style=\\\"font-size:0.9em\\\">Toll road</div>\",\"maneuver\":\"keep-left\",\"polyline\":{\"points\":\"alfgG{~v_@ZMZMFEFCFGFIBGDG@KNy@BSHu@DSDWPoARgALk@Ne@Tm@Vk@LUHMDK@CBEl@aANW~@qAt@gATY@Ct@eAPUDCPULO^c@LOPSVYfAuAPS^i@\\\\i@@A@Ad@q@d@q@FK@A?AJO@Ad@u@BC@CFILSPWXk@P]BC@CP]@A?C@ABG@ALYDMN[?A@C@ABEBMN]DSLa@Nm@BIFWDO?ALq@F]TkAD_@Fc@DY@E?CNcAD[XcBb@oCP_ADa@F_@BSHi@ZwBBOFg@BSLy@PiANiABKBUFc@@ORsALcAPkAB]?CBSBMFo@N_AL}@Jm@BSNiAD]BQD_@?C@S@W@OBy@?W?K@U@oA?Y@K?g@By@@kA?Y@U?Q@[@U@EFg@Lq@?A@GBQJu@DiAZ{ML{@J}@@E@GBKJi@BM?G\"},\"start_location\":{\"lat\":43.2916932,\"lng\":5.3657428},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"68 m\",\"value\":68},\"duration\":{\"text\":\"1 min\",\"value\":19},\"end_location\":{\"lat\":43.2824178,\"lng\":5.394057699999999},\"html_instructions\":\"Keep <b>left</b> at the fork to continue toward <b>A50</b><div style=\\\"font-size:0.9em\\\">Toll road</div>\",\"maneuver\":\"fork-left\",\"polyline\":{\"points\":\"qrdgGuj|_@De@BaA@[@a@\"},\"start_location\":{\"lat\":43.2824891,\"lng\":5.393229799999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.3 km\",\"value\":337},\"duration\":{\"text\":\"1 min\",\"value\":29},\"end_location\":{\"lat\":43.2829041,\"lng\":5.3981451},\"html_instructions\":\"Take the exit on the <b>left</b> onto <b>A50</b><div style=\\\"font-size:0.9em\\\">Partial toll road</div>\",\"maneuver\":\"ramp-left\",\"polyline\":{\"points\":\"crdgG{o|_@A[AUEq@MgAEi@GOK_ECy@A[A_@AYC_@AYA[AOAQAOCOCWCUCSEa@\"},\"start_location\":{\"lat\":43.2824178,\"lng\":5.394057699999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"31 m\",\"value\":31},\"duration\":{\"text\":\"1 min\",\"value\":4},\"end_location\":{\"lat\":43.2829091,\"lng\":5.398524099999999},\"html_instructions\":\"Take exit <b>2</b> toward <b>Baille</b>/<b>La Capelette</b>/<b>Saint Loup</b>\",\"maneuver\":\"ramp-right\",\"polyline\":{\"points\":\"cudgGmi}_@@]Ck@\"},\"start_location\":{\"lat\":43.2829041,\"lng\":5.3981451},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"52 m\",\"value\":52},\"duration\":{\"text\":\"1 min\",\"value\":7},\"end_location\":{\"lat\":43.2829114,\"lng\":5.399164000000001},\"html_instructions\":\"Keep <b>left</b> to continue toward <b>Boulevard Rabatau Daniel Matalon</b>\",\"maneuver\":\"keep-left\",\"polyline\":{\"points\":\"eudgGwk}_@?_C\"},\"start_location\":{\"lat\":43.2829091,\"lng\":5.398524099999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"32 m\",\"value\":32},\"duration\":{\"text\":\"1 min\",\"value\":6},\"end_location\":{\"lat\":43.2830094,\"lng\":5.3995291},\"html_instructions\":\"Keep <b>left</b> at the fork, follow signs for <b>La Timone</b>/<b>Baille</b>/<b>La Capelette</b>/<b>Saint-Loup</b>\",\"maneuver\":\"fork-left\",\"polyline\":{\"points\":\"eudgGwo}_@AOAKAE?EACAEAEAEACCG\"},\"start_location\":{\"lat\":43.2829114,\"lng\":5.399164000000001},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"20 m\",\"value\":20},\"duration\":{\"text\":\"1 min\",\"value\":5},\"end_location\":{\"lat\":43.28311129999999,\"lng\":5.3997302},\"html_instructions\":\"Continue onto <b>Boulevard Rabatau Daniel Matalon</b>\",\"polyline\":{\"points\":\"yudgGar}_@Sg@\"},\"start_location\":{\"lat\":43.2830094,\"lng\":5.3995291},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.1 km\",\"value\":111},\"duration\":{\"text\":\"1 min\",\"value\":18},\"end_location\":{\"lat\":43.2833256,\"lng\":5.4010622},\"html_instructions\":\"Continue straight to stay on <b>Boulevard Rabatau Daniel Matalon</b>\",\"maneuver\":\"straight\",\"polyline\":{\"points\":\"mvdgGis}_@CG?AAAEQEYEs@AIASEo@Eu@CY\"},\"start_location\":{\"lat\":43.28311129999999,\"lng\":5.3997302},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"23 m\",\"value\":23},\"duration\":{\"text\":\"1 min\",\"value\":5},\"end_location\":{\"lat\":43.2832753,\"lng\":5.401341899999999},\"html_instructions\":\"Slight <b>right</b> to stay on <b>Boulevard Rabatau Daniel Matalon</b>\",\"maneuver\":\"turn-slight-right\",\"polyline\":{\"points\":\"ywdgGs{}_@Hw@\"},\"start_location\":{\"lat\":43.2833256,\"lng\":5.4010622},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.3 km\",\"value\":336},\"duration\":{\"text\":\"1 min\",\"value\":58},\"end_location\":{\"lat\":43.281557,\"lng\":5.404732},\"html_instructions\":\"Slight <b>right</b> onto <b>Avenue de la Capelette</b>\",\"maneuver\":\"turn-slight-right\",\"polyline\":{\"points\":\"owdgGk}}_@JWDG@E@C?A?A?A@CVg@BIBGh@eAbAqB\\\\w@Zs@J[HUd@}AZ{@Vu@FQ\"},\"start_location\":{\"lat\":43.2832753,\"lng\":5.401341899999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"94 m\",\"value\":94},\"duration\":{\"text\":\"1 min\",\"value\":21},\"end_location\":{\"lat\":43.2823329,\"lng\":5.405132},\"html_instructions\":\"Turn <b>left</b> onto <b>Avenue Benjamin Delessert</b>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"wldgGqr~_@IGMIQMUOUQCAECC?EAUCYA\"},\"start_location\":{\"lat\":43.281557,\"lng\":5.404732},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"1.5 km\",\"value\":1489},\"duration\":{\"text\":\"3 mins\",\"value\":170},\"end_location\":{\"lat\":43.2849495,\"lng\":5.422878100000001},\"html_instructions\":\"Turn <b>right</b> onto <b>Boulevard Mireille Lauze</b>\",\"maneuver\":\"turn-right\",\"polyline\":{\"points\":\"qqdgGau~_@IkAGm@C_@Ga@GUCSAOCUG_CCuAAWAYCWASCUCUEWCQ?EEQGa@I_@G[K{@CQQiBEg@@mB?a@ASKwBEs@Cu@Ey@Ei@Ci@AMGc@Ga@EWAMCe@AI@e@?K?[?c@?KAYAWC]MwA?Y?{DBuBAaAAkAKgBIk@AMG[Ke@WgAY_Ae@_Bi@iB]eAw@eCCGC_@Cg@?K?CAK?S?qD\"},\"start_location\":{\"lat\":43.2823329,\"lng\":5.405132},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.3 km\",\"value\":330},\"duration\":{\"text\":\"1 min\",\"value\":46},\"end_location\":{\"lat\":43.2842905,\"lng\":5.4265526},\"html_instructions\":\"Slight <b>right</b> toward <b>Rue d'André Bardon</b>\",\"maneuver\":\"turn-slight-right\",\"polyline\":{\"points\":\"}aegG_db`@FYBa@@e@BWDMDOFMDI@G@C?GAGAICEGOEICKCKCQ?Q?o@?o@@c@@Y@YBYHK@C@EPu@Ho@@CBGDEJQV_@FIHO\"},\"start_location\":{\"lat\":43.2849495,\"lng\":5.422878100000001},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"13 m\",\"value\":13},\"duration\":{\"text\":\"1 min\",\"value\":1},\"end_location\":{\"lat\":43.2841888,\"lng\":5.4266269},\"html_instructions\":\"Slight <b>right</b> onto <b>Rue d'André Bardon</b>\",\"maneuver\":\"turn-slight-right\",\"polyline\":{\"points\":\"y}dgG}zb`@RO\"},\"start_location\":{\"lat\":43.2842905,\"lng\":5.4265526},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":218},\"duration\":{\"text\":\"1 min\",\"value\":26},\"end_location\":{\"lat\":43.2822477,\"lng\":5.4269568},\"html_instructions\":\"Slight <b>right</b> to stay on <b>Rue d'André Bardon</b>\",\"maneuver\":\"turn-slight-right\",\"polyline\":{\"points\":\"e}dgGm{b`@B@B?DANGXGXEd@KVGRCPAF?PAF?jCK^C\"},\"start_location\":{\"lat\":43.2841888,\"lng\":5.4266269},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":186},\"duration\":{\"text\":\"1 min\",\"value\":31},\"end_location\":{\"lat\":43.2835131,\"lng\":5.4269891},\"html_instructions\":\"Turn <b>left</b> onto <b>Avenue Florian</b>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"aqdgGo}b`@JG@CBC?C@G@GAE?EAC?AAAACACCCECQIyCl@i@JYFMH\"},\"start_location\":{\"lat\":43.2822477,\"lng\":5.4269568},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":162},\"duration\":{\"text\":\"1 min\",\"value\":22},\"end_location\":{\"lat\":43.2848203,\"lng\":5.4261589},\"html_instructions\":\"Continue straight onto <b>Rue d'André Bardon</b>\",\"maneuver\":\"straight\",\"polyline\":{\"points\":\"}xdgGu}b`@m@Lm@LYJ[P[XWV_Ax@\"},\"start_location\":{\"lat\":43.2835131,\"lng\":5.4269891},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"25 m\",\"value\":25},\"duration\":{\"text\":\"1 min\",\"value\":5},\"end_location\":{\"lat\":43.2850448,\"lng\":5.4261244},\"html_instructions\":\"Take the ramp to <b>Aubagne</b>/<b>Toulon</b>/<b>Nice</b>/<b>La Valentine</b>\",\"polyline\":{\"points\":\"caegGoxb`@[DG@E?A?\"},\"start_location\":{\"lat\":43.2848203,\"lng\":5.4261589},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":159},\"duration\":{\"text\":\"1 min\",\"value\":29},\"end_location\":{\"lat\":43.2864035,\"lng\":5.425542999999999},\"html_instructions\":\"Slight <b>left</b> toward <b>Avenue Désiré Bianco</b>\",\"maneuver\":\"turn-slight-left\",\"polyline\":{\"points\":\"obegGgxb`@CBA@A@E@A?C?C@A@mB|@gAVc@F[F\"},\"start_location\":{\"lat\":43.2850448,\"lng\":5.4261244},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":170},\"duration\":{\"text\":\"1 min\",\"value\":25},\"end_location\":{\"lat\":43.2878751,\"lng\":5.425175299999999},\"html_instructions\":\"Merge onto <b>Avenue Désiré Bianco</b><div style=\\\"font-size:0.9em\\\">Destination will be on the right</div>\",\"maneuver\":\"merge\",\"polyline\":{\"points\":\"_kegGstb`@A?I@KFKDQNMLGDGBGBIBM@g@@}@@W@i@?K?\"},\"start_location\":{\"lat\":43.2864035,\"lng\":5.425542999999999},\"travel_mode\":\"DRIVING\"}],\"traffic_speed_entry\":[],\"via_waypoint\":[]},{\"distance\":{\"text\":\"8.0 km\",\"value\":7970},\"duration\":{\"text\":\"17 mins\",\"value\":1034},\"end_address\":\"Avenue des Jas des Candolles, 13821 La Penne-sur-Huveaune, France\",\"end_location\":{\"lat\":43.2805498,\"lng\":5.510650399999999},\"start_address\":\"38 Avenue Désiré Bianco, 13011 Marseille, France\",\"start_location\":{\"lat\":43.2878751,\"lng\":5.425175299999999},\"steps\":[{\"distance\":{\"text\":\"0.1 km\",\"value\":135},\"duration\":{\"text\":\"1 min\",\"value\":25},\"end_location\":{\"lat\":43.2889619,\"lng\":5.4257536},\"html_instructions\":\"Head <b>north</b> on <b>Avenue Désiré Bianco</b> toward <b>Rue Dupré</b>\",\"polyline\":{\"points\":\"gtegGkrb`@[@C?W?QAOEOGMMGGEGIKEIMOIGEEECKEKGE?\"},\"start_location\":{\"lat\":43.2878751,\"lng\":5.425175299999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"71 m\",\"value\":71},\"duration\":{\"text\":\"1 min\",\"value\":14},\"end_location\":{\"lat\":43.2888922,\"lng\":5.426628},\"html_instructions\":\"<b>Avenue Désiré Bianco</b> turns <b>right</b> and becomes <b>Rue Dupré</b>\",\"polyline\":{\"points\":\"_{egG}ub`@JwC@W\"},\"start_location\":{\"lat\":43.2889619,\"lng\":5.4257536},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.1 km\",\"value\":113},\"duration\":{\"text\":\"1 min\",\"value\":27},\"end_location\":{\"lat\":43.2898763,\"lng\":5.4263446},\"html_instructions\":\"Turn <b>left</b> onto <b>Avenue Mazenode</b>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"qzegGm{b`@EAG?OAI?O@UBMBUFG@OFYLa@R\"},\"start_location\":{\"lat\":43.2888922,\"lng\":5.426628},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"1.0 km\",\"value\":973},\"duration\":{\"text\":\"2 mins\",\"value\":129},\"end_location\":{\"lat\":43.2893073,\"lng\":5.437925799999999},\"html_instructions\":\"Sharp <b>right</b> onto <b>Avenue Jean Lombard</b>\",\"maneuver\":\"turn-sharp-right\",\"polyline\":{\"points\":\"w`fgGsyb`@dAaCRa@FQZg@@AHUH[DWDOHg@Hu@Dk@@S?S?a@@U?m@Au@?wA?aAA_A?{@?uDAQ?_AC{D?[ACCk@?O?yA?cAC_@Gm@Ec@UsBMiBEk@C{@?yA?sB@I@Q?C?K\"},\"start_location\":{\"lat\":43.2898763,\"lng\":5.4263446},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.3 km\",\"value\":276},\"duration\":{\"text\":\"1 min\",\"value\":52},\"end_location\":{\"lat\":43.2900563,\"lng\":5.4410146},\"html_instructions\":\"At the roundabout, continue straight onto <b>Avenue Emmanuel Allard</b>\",\"maneuver\":\"roundabout-right\",\"polyline\":{\"points\":\"e}egGabe`@B?@?@A@A@C?A@A?I?EAA?CACAAAAAACAAACe@G{@CYAME_@AEAEQc@g@{AOg@I[c@}D\"},\"start_location\":{\"lat\":43.2893073,\"lng\":5.437925799999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.5 km\",\"value\":485},\"duration\":{\"text\":\"1 min\",\"value\":66},\"end_location\":{\"lat\":43.2858464,\"lng\":5.442561299999999},\"html_instructions\":\"Turn <b>right</b> onto <b>Avenue du Dr Heckel</b>/<b>D2A</b>\",\"maneuver\":\"turn-right\",\"polyline\":{\"points\":\"{afgGiue`@|@WbCq@FCXGBAh@MDAFCVIFA@?PGDAJCJCHABABADA@?JC@AJC@?DAt@QlAYBAzD{@`@Mt@_@\"},\"start_location\":{\"lat\":43.2900563,\"lng\":5.4410146},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"5.5 km\",\"value\":5480},\"duration\":{\"text\":\"11 mins\",\"value\":644},\"end_location\":{\"lat\":43.2825367,\"lng\":5.5077795},\"html_instructions\":\"Turn <b>left</b> onto <b>Boulevard de la Valbarelle</b>/<b>D8N</b><div style=\\\"font-size:0.9em\\\">Continue to follow D8N</div>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"qgegG__f`@{AoGk@aC_AwDU}@?I?CCMSw@EWOo@AMIy@CKAMQsAIq@AOGi@AGOaAGc@G_@CKKi@CMACEEg@iCOu@Ic@Is@ACe@aEAMa@eD?KAK?IAQAmBAuAEaFA}A?o@?GCwCEyBK{GEsCAsAAi@?S?A?m@CsAA{@AeABm@Bc@?CBOBQH_@H]Nm@H]V}@DSBK?A@CBE@GBKVeAXiA\\\\uAj@wBJe@~@qDBM@E?CBGBKNm@T_ANi@Ng@Jc@x@{CHa@BWF_@Dc@B[@WBc@?W?]@{@?wA@]?i@@q@?wB@oA?GB_G?K?E?_A@gB?_@?eAA{@AQ?k@@s@?S@qA?cA@eAB_A?Q@SBa@Bi@Hy@@O@KFaAHeA@QBUJaADe@Ho@@I@OD[Fa@@QFc@R}Ab@sDHk@Fe@DYBU@I@OBOBKBMTaALe@l@cCNq@DU@G@G?E@I?I?E?K?WAm@?aAAgA?m@AwF?qB?M?I?E?E@G@SNcAFe@DWDYDUDUHc@Hc@DSBSB[@ODc@HmAHsAVaEJeBFgADI?GB]?CFaAB]HuA@QFs@Fo@NmA@O@MB_@JcABWDc@J_ADi@LuA@KBi@@_A?]AM?MAK?EAO?CKkAIq@G_AC[CYCYAa@CY?S?UAq@?wAAa@CyCAk@C}CAk@?O?Q@a@BWFw@F{@BSFo@J{@Hq@L}@@O?AF[@m@BaA?Q?{C?AAY?S@U?Q?OBUBQBQDQDQDODKDMHMHKLOPOLOBABE@C\"},\"start_location\":{\"lat\":43.2858464,\"lng\":5.442561299999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":221},\"duration\":{\"text\":\"1 min\",\"value\":29},\"end_location\":{\"lat\":43.2807626,\"lng\":5.508398199999999},\"html_instructions\":\"Turn <b>right</b> onto <b>Boulevard des Candolles</b>\",\"maneuver\":\"turn-right\",\"polyline\":{\"points\":\"{rdgGsvr`@DFBD?@@@B@@?B?@?@?BA@A@CDCBCHIHKZc@h@o@BEDCDCHCHAH?F?\\\\D\\\\BD@DAD?DAF?^Gj@A\"},\"start_location\":{\"lat\":43.2825367,\"lng\":5.5077795},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.1 km\",\"value\":119},\"duration\":{\"text\":\"1 min\",\"value\":21},\"end_location\":{\"lat\":43.2809031,\"lng\":5.509737900000001},\"html_instructions\":\"Turn <b>left</b> onto <b>Avenue des Jas des Candolles</b><div style=\\\"font-size:0.9em\\\">Restricted usage road</div>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"wgdgGozr`@FgC@u@?OAMCICICIEGGEIC\"},\"start_location\":{\"lat\":43.2807626,\"lng\":5.508398199999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"41 m\",\"value\":41},\"duration\":{\"text\":\"1 min\",\"value\":14},\"end_location\":{\"lat\":43.2808655,\"lng\":5.5102414},\"html_instructions\":\"Turn <b>right</b><div style=\\\"font-size:0.9em\\\">Restricted usage road</div>\",\"maneuver\":\"turn-right\",\"polyline\":{\"points\":\"shdgG{bs`@Bo@@s@\"},\"start_location\":{\"lat\":43.2809031,\"lng\":5.509737900000001},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"56 m\",\"value\":56},\"duration\":{\"text\":\"1 min\",\"value\":13},\"end_location\":{\"lat\":43.2805498,\"lng\":5.510650399999999},\"html_instructions\":\"Turn <b>right</b><div style=\\\"font-size:0.9em\\\">Restricted usage road</div>\",\"maneuver\":\"turn-right\",\"polyline\":{\"points\":\"mhdgG_fs`@D@@@@@@?B@@?B?@A@CBCBGFQTu@\"},\"start_location\":{\"lat\":43.2808655,\"lng\":5.5102414},\"travel_mode\":\"DRIVING\"}],\"traffic_speed_entry\":[],\"via_waypoint\":[]},{\"distance\":{\"text\":\"6.5 km\",\"value\":6491},\"duration\":{\"text\":\"10 mins\",\"value\":588},\"end_address\":\"25 Avenue Loulou Delfieu, 13400 Aubagne, France\",\"end_location\":{\"lat\":43.2926392,\"lng\":5.567589},\"start_address\":\"Avenue des Jas des Candolles, 13821 La Penne-sur-Huveaune, France\",\"start_location\":{\"lat\":43.2805498,\"lng\":5.510650399999999},\"steps\":[{\"distance\":{\"text\":\"56 m\",\"value\":56},\"duration\":{\"text\":\"1 min\",\"value\":10},\"end_location\":{\"lat\":43.2808655,\"lng\":5.5102414},\"html_instructions\":\"Head <b>northwest</b><div style=\\\"font-size:0.9em\\\">Restricted usage road</div>\",\"polyline\":{\"points\":\"mfdgGqhs`@Ut@GPCFCBABA@C?A?CAA?AAAAEA\"},\"start_location\":{\"lat\":43.2805498,\"lng\":5.510650399999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"41 m\",\"value\":41},\"duration\":{\"text\":\"1 min\",\"value\":11},\"end_location\":{\"lat\":43.2809031,\"lng\":5.509737900000001},\"html_instructions\":\"Turn <b>left</b> toward <b>Avenue des Jas des Candolles</b><div style=\\\"font-size:0.9em\\\">Restricted usage road</div>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"mhdgG_fs`@Ar@Cn@\"},\"start_location\":{\"lat\":43.2808655,\"lng\":5.5102414},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.1 km\",\"value\":119},\"duration\":{\"text\":\"1 min\",\"value\":21},\"end_location\":{\"lat\":43.2807626,\"lng\":5.508398199999999},\"html_instructions\":\"Turn <b>left</b> onto <b>Avenue des Jas des Candolles</b><div style=\\\"font-size:0.9em\\\">Restricted usage road</div>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"shdgG{bs`@HBFDDFBHBHBH@L?NAt@GfC\"},\"start_location\":{\"lat\":43.2809031,\"lng\":5.509737900000001},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":221},\"duration\":{\"text\":\"1 min\",\"value\":39},\"end_location\":{\"lat\":43.2825367,\"lng\":5.5077795},\"html_instructions\":\"Turn <b>right</b> onto <b>Boulevard des Candolles</b>\",\"maneuver\":\"turn-right\",\"polyline\":{\"points\":\"wgdgGozr`@k@@_@FG?E@E?E@EA]C]EG?I?I@IBEBEBCDi@n@[b@IJIHCBEBABA@C@A?A?C?A?CAAA?ACEEG\"},\"start_location\":{\"lat\":43.2807626,\"lng\":5.508398199999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.6 km\",\"value\":600},\"duration\":{\"text\":\"1 min\",\"value\":66},\"end_location\":{\"lat\":43.2816925,\"lng\":5.5150062},\"html_instructions\":\"Turn <b>right</b> onto <b>Boulevard Voltaire</b>/<b>D8N</b>\",\"maneuver\":\"turn-right\",\"polyline\":{\"points\":\"{rdgGsvr`@R]FMFODOBKDWBO@I@Q?UFk@Fu@LaBDo@BWJwAFcABg@BO@[@MFkA@i@HcBB}@@W@W@I?GAe@?mA?s@A{A?I?Y?K?MBW@S@OBQ\"},\"start_location\":{\"lat\":43.2825367,\"lng\":5.5077795},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":229},\"duration\":{\"text\":\"1 min\",\"value\":42},\"end_location\":{\"lat\":43.28375399999999,\"lng\":5.515034500000001},\"html_instructions\":\"Turn <b>left</b> onto <b>Boulevard de la Gare</b>/<b>D2E</b>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"qmdgGyct`@o@EkB?_@@o@?a@?eAAs@?S@\"},\"start_location\":{\"lat\":43.2816925,\"lng\":5.5150062},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"85 m\",\"value\":85},\"duration\":{\"text\":\"1 min\",\"value\":12},\"end_location\":{\"lat\":43.2838006,\"lng\":5.5143105},\"html_instructions\":\"Turn <b>left</b> to stay on <b>Boulevard de la Gare</b>/<b>D2E</b>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"mzdgG}ct`@g@@PT@B@B@B?HBl@@T?\\\\\"},\"start_location\":{\"lat\":43.28375399999999,\"lng\":5.515034500000001},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":155},\"duration\":{\"text\":\"1 min\",\"value\":18},\"end_location\":{\"lat\":43.2851598,\"lng\":5.514177999999999},\"html_instructions\":\"At the roundabout, take the <b>1st</b> exit and stay on <b>Boulevard de la Gare</b>/<b>D2E</b>\",\"maneuver\":\"roundabout-right\",\"polyline\":{\"points\":\"wzdgGm_t`@A??@A?A@?@A??@A??@?@A??@?@KDC@C@C?A?E?E?Y?wB@M?e@?WA\"},\"start_location\":{\"lat\":43.2838006,\"lng\":5.5143105},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"2.6 km\",\"value\":2577},\"duration\":{\"text\":\"2 mins\",\"value\":115},\"end_location\":{\"lat\":43.2877495,\"lng\":5.5455653},\"html_instructions\":\"Turn <b>right</b> to merge onto <b>A50</b> toward <b>Aubagne</b>/<b>Toulon</b>/<b>Nice</b>\",\"polyline\":{\"points\":\"gcegGs~s`@JaCDaAFwABc@Be@@a@Dw@@_@DeA@Y@UB]Dw@JcB?EAAG[Fo@@W@IF_AB_@Bq@Bg@B_@@s@@m@B}@?c@?oA?y@A}@C}@Ag@E{@Ey@E{@WgDEs@UaDu@mKQiCK}A[{DS{CO}Bg@aHOwBWuDk@aIYkEUuCIkA[yEiAePWuDe@}GWkDKeBAOCOMqB\"},\"start_location\":{\"lat\":43.2851598,\"lng\":5.514177999999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.8 km\",\"value\":835},\"duration\":{\"text\":\"1 min\",\"value\":36},\"end_location\":{\"lat\":43.29085430000001,\"lng\":5.5547585},\"html_instructions\":\"Keep <b>right</b> at the fork to continue on <b>A501</b>, follow signs for <b>Nice</b>/<b>Aix-en-Provence</b>/<b>Aubagne-Centre</b>\",\"maneuver\":\"fork-right\",\"polyline\":{\"points\":\"msegGybz`@?c@A[Ek@Gs@C_@Gu@Gs@Gk@E_@MgACOCQEWGa@G_@EWAEIc@Ki@Qw@WiA[yA{AeH[wAOk@Og@Oc@Yy@m@_BQ]_@u@Wg@Ue@OUGICE}@sA\"},\"start_location\":{\"lat\":43.2877495,\"lng\":5.5455653},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.5 km\",\"value\":469},\"duration\":{\"text\":\"1 min\",\"value\":34},\"end_location\":{\"lat\":43.2942573,\"lng\":5.558067599999999},\"html_instructions\":\"Take exit <b>6</b> toward <b>Aubagne - Centre</b>\",\"maneuver\":\"ramp-right\",\"polyline\":{\"points\":\"yffgGg|{`@K]GKKMU[SS[]_A_AOOOMSOWS_@Y]Ue@Y_@UWS]Y]W_Au@w@o@s@k@IIAAGKEECGCEEEAGAEEKAI\"},\"start_location\":{\"lat\":43.29085430000001,\"lng\":5.5547585},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.5 km\",\"value\":519},\"duration\":{\"text\":\"1 min\",\"value\":88},\"end_location\":{\"lat\":43.294694,\"lng\":5.5640563},\"html_instructions\":\"At the roundabout, take the <b>2nd</b> exit onto <b>Avenue Antide Boyer</b>/<b>D2</b>\",\"maneuver\":\"roundabout-right\",\"polyline\":{\"points\":\"c|fgG}p|`@@A?A@A?A?A?A?A?A?A?A?A?A?A?A?A?AAA?A?AA??A?AA??AA??AA??A@WBM?CBMBO@A?MDQBQFi@Ae@EUEOGQe@cAWk@Sa@?AMUAEIOCGACAEEMES?AEWEg@?[@q@@i@HkBBe@@Q@SP{DD}@@Y\"},\"start_location\":{\"lat\":43.2942573,\"lng\":5.558067599999999},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":214},\"duration\":{\"text\":\"1 min\",\"value\":36},\"end_location\":{\"lat\":43.2929096,\"lng\":5.5630799},\"html_instructions\":\"Turn <b>right</b> onto <b>Avenue Simon Lagunas</b>/<b>D42A</b>\",\"maneuver\":\"turn-right\",\"polyline\":{\"points\":\"y~fgGkv}`@J?TDj@Vl@VvCzAFDHFJDDBj@X\"},\"start_location\":{\"lat\":43.294694,\"lng\":5.5640563},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.4 km\",\"value\":371},\"duration\":{\"text\":\"1 min\",\"value\":60},\"end_location\":{\"lat\":43.2926392,\"lng\":5.567589},\"html_instructions\":\"Turn <b>left</b> onto <b>Avenue des Goums</b>/<b>D8N</b><div style=\\\"font-size:0.9em\\\">Continue to follow D8N</div>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"usfgGgp}`@Jk@NoAVgCD_@Dc@@O@I?G@UDSAi@Ai@Aa@IaHEyA\"},\"start_location\":{\"lat\":43.2929096,\"lng\":5.5630799},\"travel_mode\":\"DRIVING\"}],\"traffic_speed_entry\":[],\"via_waypoint\":[]}],\"overview_polyline\":{\"points\":\"ogggGuxw_@`@hJv@nId@nF?LTbCVjCAn@Gj@GRYb@[TiAVcA?_BK?ACEICIBCD_Ce@uE_AeNgDaEw@eGuASGG`@d@HHLr@T|A`@lEZr@?ZIpCf@tB^bMnCtCz@hAP~AAbASz@W|@c@VSlJqIjBeApDwAr@_@Re@b@wCx@{Ed@sAd@aATc@rDsFdDgEv@}@vC}DdBgCtAuBrBkE`AaDfAiGlBwLfAcIxC{T|@}GL_GHuGL_BRmAP_CZ{ML{@LcADSTeBD{C[yDGOOyFIuBGwAMmAIu@AiAEaDEUg@wAOkBKeBDqATi@@IhA_ChCyFbBeFFQIG_@Wu@g@y@G]{DQoAOgGMwAu@{EU{BCuCSaFScEWkBEcB?wAUgD@mKMsDKy@eAiEeDuKK{AAuEPyBXu@@[Si@K{@B}CDs@JO^oBl@_APYVMr@QjB]r@CjDOLKFWM_@WMcEx@g@P{AZu@\\\\s@p@_Ax@[DM@IFOBoB~@kB^s@Ps@h@g@LkFFa@G]U]e@c@a@WME?JwC@WEAWA}@HgA^a@RdAaCZs@\\\\i@Rq@^eCFuB?qEG}SEyCCcBMqAc@}EIgBBiFDODI?SEKGEQiC[oAw@cCm@yEbFuAz@UzA_@j@OlCo@|EiAt@_@{AoGkByHUgA]aB_@cC_@cDa@wC[cB_AiE{@}Ge@kEE_FGwJ[aUKwKJeBf@}Br@oCnGeW~AiGTwBDqBB{EFsQ?oKF{GHqBp@gIbCqSrB}IDsAEgRf@cEn@gEp@gKX_Ej@mIx@kIXuD?yBe@_GMoBAsDKeK?oBV_Dj@{EJ{ABqF?uAJiAV_Af@w@f@g@@CDFBFJBHChB{B^MlAHVAf@Gj@AFgC@eAEWUa@ICBo@@s@D@BBD@HEd@sAe@rAIDIEEAAr@Cn@HBLLFRB|AGfCk@@g@FW@mAI_@LiBzBIBKCCGEGR]N]H[LcA\\\\yEh@eI\\\\yICaIJmAo@EkB?oA@oD?g@@PTBFFpAA^CBCDULwE@WAJaCLyCNcD`@uII]HgAP{CJiDBkFM_Fi@qIqEoo@sF}w@aBeVSuDq@qIs@aFcFsUgBeF_BaDyAyBSi@a@i@_CaC{AkA{ByAsDwCgAcASYKc@BE?M?KEICCDg@Z_CG{@Ma@qAsCm@_BK}APmFZ_H@YJ?`A\\\\vE`CPHj@XJk@f@wEJcADw@B}@CkAO{J\"},\"summary\":\"Tunnel Prado-Carénage\",\"warnings\":[],\"waypoint_order\":[1,0]}],\"status\":\"OK\"}");
        Routes routes = GoogleParser.getGson().fromJson(reader, Routes.class);

        Log.i("route", routes.getStatus());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        polylines = new ArrayList<>();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        MapsInitializer.initialize(this);
        mGoogleApiClient.connect();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            getSupportFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);

        mAdapter = new PlaceAutoCompleteAdapter(this, android.R.layout.simple_list_item_1,
                mGoogleApiClient, BOUNDS_JAMAICA, null);








        /*
         * Adds auto complete adapter to both auto complete
         * text views.
         * */
        starting.setAdapter(mAdapter);
        destination.setAdapter(mAdapter);


        /*
         * Sets the start and destination points based on the values selected
         * from the autocomplete text views.
         * */

        starting.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                final String placeId = String.valueOf(item.placeId);
                Log.i(LOG_TAG, "Autocomplete item selected: " + item.description);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
              details about the place.
              */
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (!places.getStatus().isSuccess()) {
                            // Request did not complete successfully
                            Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                            places.release();
                            return;
                        }
                        // Get the Place object from the buffer.
                        final Place place = places.get(0);

                        start = place.getLatLng();
                    }
                });

            }
        });
        destination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                final String placeId = String.valueOf(item.placeId);
                Log.i(LOG_TAG, "Autocomplete item selected: " + item.description);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
              details about the place.
              */
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (!places.getStatus().isSuccess()) {
                            // Request did not complete successfully
                            Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                            places.release();
                            return;
                        }
                        // Get the Place object from the buffer.
                        final Place place = places.get(0);

                        end = place.getLatLng();
                    }
                });

            }
        });

        /*
        These text watchers set the start and end points to null because once there's
        * a change after a value has been selected from the dropdown
        * then the value has to reselected from dropdown to get
        * the correct location.
        * */
        starting.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int startNum, int before, int count) {
                if (start != null) {
                    start = null;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        destination.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (end != null) {
                    end = null;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @OnClick(R.id.send)
    public void sendRequest() {
        if (Util.Operations.isOnline(this)) {
            route();
        } else {
            Toast.makeText(this, "No internet connectivity", Toast.LENGTH_SHORT).show();
        }
    }

    public void route() {
        if (start == null || end == null) {
            if (start == null) {
                if (starting.getText().length() > 0) {
                    starting.setError("Choose location from dropdown.");
                } else {
                    Toast.makeText(this, "Please choose a starting point.", Toast.LENGTH_SHORT).show();
                }
            }
            if (end == null) {
                if (destination.getText().length() > 0) {
                    destination.setError("Choose location from dropdown.");
                } else {
                    Toast.makeText(this, "Please choose a destination.", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            progressDialog = ProgressDialog.show(this, "Please wait.",
                    "Fetching route information.", true);
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(start, end)
                    .build();
            routing.execute();
        }
    }


    @Override
    public void onRoutingFailure(RouteException e) {
        // The Routing request failed
        progressDialog.dismiss();
        if (e != null) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
        Log.e(LOG_TAG, "onRoutingFailure", e);
    }

    @Override
    public void onRoutingStart() {
        // The Routing Request starts
    }

    @Override
    public void onRoutingSuccess(Routes routes) {
        List<Route> route = routes.getRoutes();
        progressDialog.dismiss();
        CameraUpdate center = CameraUpdateFactory.newLatLng(start);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

        map.moveCamera(center);


        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;
            int color = getResources().getColor(COLORS[colorIndex]);

            PolylineOptions overviewPolyline = route.get(i).getOverviewPolyline();
            overviewPolyline.color(color);
            overviewPolyline.width(10 + i * 3);
            Polyline polyline = map.addPolyline(overviewPolyline);
            polylines.add(polyline);

            //Toast.makeText(getApplicationContext(),"Route "+ (i+1) +": distance - "+ route.get(i).getDistanceValue()+": duration - "+ route.get(i).getDurationValue(),Toast.LENGTH_SHORT).show();
        }

        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(start);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.start_blue));
        map.addMarker(options);

        // End marker
        options = new MarkerOptions();
        options.position(end);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.end_green));
        map.addMarker(options);

    }

    @Override
    public void onRoutingCancelled() {
        Log.i(LOG_TAG, "Routing was cancelled.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.v(LOG_TAG, connectionResult.toString());
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;/*
         * Updates the bounds being used by the auto complete adapter based on the position of the
         * map.
         * */
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {
                LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
                mAdapter.setBounds(bounds);
            }
        });


        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(18.013610, -77.498803));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

        map.moveCamera(center);
        map.animateCamera(zoom);
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 5000, 0,
                new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

                        map.moveCamera(center);
                        map.animateCamera(zoom);
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                });


        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                3000, 0, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),location.getLongitude()));
                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

                        map.moveCamera(center);
                        map.animateCamera(zoom);

                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                });
    }
}
