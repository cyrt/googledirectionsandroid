package com.directions.route;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by cyril on 24/08/2016.
 */
public class Step {
    private LatLng start_location;
    private LatLng end_location;
    private TextIntPair distance;
    private TextIntPair duration;

    private String html_instruction;
    private PolylineOptions polyline;
    private String travel_mode;

    public LatLng getStartLocation() {
        return start_location;
    }

    public void setStartLocation(LatLng start_location) {
        this.start_location = start_location;
    }

    public LatLng getEndLocation() {
        return end_location;
    }

    public void setEndLocation(LatLng end_location) {
        this.end_location = end_location;
    }

    public TextIntPair getDistance() {
        return distance;
    }

    public void setDistance(TextIntPair distance) {
        this.distance = distance;
    }

    public TextIntPair getDuration() {
        return duration;
    }

    public void setDuration(TextIntPair duration) {
        this.duration = duration;
    }

    public String getHtmlInstruction() {
        return html_instruction;
    }

    public void setHtmlInstruction(String html_instruction) {
        this.html_instruction = html_instruction;
    }

    public PolylineOptions getPolyline() {
        return polyline;
    }

    public void setPolyline(PolylineOptions polyline) {
        this.polyline = polyline;
    }

    public String getTravelMode() {
        return travel_mode;
    }

    public void setTravelMode(String travel_mode) {
        this.travel_mode = travel_mode;
    }
}
