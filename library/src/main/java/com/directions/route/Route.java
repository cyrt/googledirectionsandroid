package com.directions.route;
//by Haseem Saheed

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class Route {
    private LatLngBounds bounds;
    private String copyright;
    private ArrayList<Leg> legs;
    private PolylineOptions overview_polyline;
    private String summary;
    private String[] warnings;
    private int[] waypoint_order;

    private PolylineOptions polyline;

    /**
     * @param summary the name to set
     */
    public void setSummary(final String summary) {
        this.summary = summary;
    }

    /**
     * @return the name
     */
    public String getSummary() {
        return summary;
    }

    /**
     * @param copyright the copyright to set
     */
    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    /**
     * @return the copyright
     */
    public String getCopyright() {
        return copyright;
    }

    /**
     * @param warnings the warnings to set
     */
    public void setWarnings(String[] warnings) {
        this.warnings = warnings;
    }

    /**
     * @return the warnings
     */
    public String[] getWarnings() {
        return warnings;
    }


    /**
     * @return the LatLngBounds object to map camera
     */
    public LatLngBounds getBounds() {
        return bounds;
    }

    public void setBounds(LatLngBounds bounds) {
        this.bounds = bounds;
    }

    public void setLatLgnBounds(LatLng northeast, LatLng southwest) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(northeast);
        builder.include(southwest);
        this.bounds = builder.build();
    }

    public void setWaypointOrder(int[] waypoint_order) {
        this.waypoint_order = waypoint_order;
    }

    public int[] getWaypointOrder() {
        return waypoint_order;
    }

    public void setLegs(ArrayList<Leg> legs) {
        this.legs = legs;
    }

    public ArrayList<Leg> getLegs() {
        return legs;
    }

    public void setOverviewPolyline(PolylineOptions overview_polyline) {
        this.overview_polyline = overview_polyline;
    }

    public PolylineOptions getOverviewPolyline() {
        return overview_polyline;
    }

    public PolylineOptions getDetailledPolyline() {
        if (null == polyline) {
            polyline = new PolylineOptions();
            for (Leg l : legs) {
                polyline.addAll(l.getPolyline().getPoints());
            }
        }
        return polyline;
    }

    public int getTotalDuration() {
        int total = 0;
        for (Leg l : legs) {
            total += l.getDuration().getValue();
        }
        return total;
    }

    public int getTotalDistance() {
        int total = 0;
        for (Leg l : legs) {
            total += l.getDistance().getValue();
        }
        return total;
    }
}

