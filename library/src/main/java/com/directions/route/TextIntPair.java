package com.directions.route;

/**
 * Created by cyril on 24/08/2016.
 */
public class TextIntPair {
    private String text;
    private int value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
