package com.directions.route;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cyril on 24/08/2016.
 */
public class Routes {
    List<Route> routes;
    String status;

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public Route getShortest() {
        if (routes.isEmpty()) {
            return null;
        }
        if (routes.size() == 1) {
            return routes.get(0);
        }
        int min = Integer.MAX_VALUE;
        int best = 0;
        for (int i = 0; i < routes.size(); i++) {
            Route r = routes.get(i);
            int value = r.getTotalDistance();
            if (value < min) {
                min = value;
                best = i;
            }
        }
        return routes.get(best);
    }

    public Route getFastest() {
        if (routes.isEmpty()) {
            return null;
        }
        if (routes.size() == 1) {
            return routes.get(0);
        }
        int min = Integer.MAX_VALUE;
        int best = 0;
        for (int i = 0; i < routes.size(); i++) {
            Route r = routes.get(i);
            int value = r.getTotalDistance();
            if (value < min) {
                min = value;
                best = i;
            }
        }
        return routes.get(best);
    }
}
