package com.directions.route;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nelson on 1/13/16.
 */
public class RouteException extends Exception {
    private String url;

    public RouteException(String url, String msg){
        super(msg);
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
