package com.directions.route;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class GoogleParser extends XMLParser implements Parser {

    private static final String VALUE = "value";
    private static final String DISTANCE = "distance";
    /**
     * Distance covered. *
     */
    private int distance;

    /* Status code returned when the request succeeded */
    private static final String OK = "OK";

    public GoogleParser(String feedUrl) {
        super(feedUrl);
    }

    /**
     * Parses a url pointing to a Google JSON object to a Route object.
     *
     * @return a Route object based on the JSON object by Haseem Saheed
     */
    public final Routes parse() throws RouteException {
        InputStreamReader reader = null;
        try {
            reader = new InputStreamReader(getInputStream());
            Routes routes = getGson().fromJson(reader, Routes.class);
            if (!"OK".equals(routes.status)) {
                throw new RouteException(feedUrl, routes.status);
            }
            return routes;
        } catch (Exception e) {
            throw new RouteException(feedUrl, e.getMessage());
        } finally {
            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException ignored) {
                }
            }
        }
    }

    public static final Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LatLng.class, new TypeAdapter<LatLng>() {
            @Override
            public void write(JsonWriter out, LatLng value) throws IOException {
                out.beginObject();
                out.name("lat").value(value.latitude);
                out.name("lng").value(value.longitude);
                out.endObject();
            }

            @Override
            public LatLng read(JsonReader in) throws IOException {
                double lat = -1;
                double lng = -1;
                in.beginObject();
                while (in.hasNext()) {
                    switch (in.nextName()) {
                        case "lat": lat = in.nextDouble(); break;
                        case "lng": lng = in.nextDouble(); break;
                    }
                }
                in.endObject();
                return new LatLng(lat, lng);
            }
        });
        builder.registerTypeAdapter(PolylineOptions.class, new TypeAdapter<PolylineOptions>() {
            @Override
            public void write(JsonWriter out, PolylineOptions value) throws IOException {
                out.beginObject();
                out.name("__unsupported__").value("<sorry, writing PolylineOptions is not implemented.>");
                // ?
                out.endObject();
            }

            @Override
            public PolylineOptions read(JsonReader in) throws IOException {
                PolylineOptions polylineOptions = new PolylineOptions();
                in.beginObject();
                while (in.hasNext()) {
                    switch (in.nextName()) {
                        case "points":
                            List<LatLng> latLngs = decodePolyLine(in.nextString());
                            polylineOptions.addAll(latLngs);
                            break;
                    }
                }
                in.endObject();
                return polylineOptions;
            }
        });
        return builder.create();
    }

    /**
     * Decode a polyline string into a list of GeoPoints.
     *
     * @param poly polyline encoded string to decode.
     * @return the list of GeoPoints represented by this polystring.
     */

    private static List<LatLng> decodePolyLine(final String poly) {
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng(
                    lat / 100000d, lng / 100000d
            ));
        }

        return decoded;
    }
}
