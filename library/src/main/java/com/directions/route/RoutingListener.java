package com.directions.route;

import java.util.List;

public interface RoutingListener {
    void onRoutingFailure(RouteException e);

    void onRoutingStart();

    void onRoutingSuccess(Routes routes);

    void onRoutingCancelled();
}
