package com.directions.route;
//by Haseem Saheed

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class XMLParser {
    // names of the XML tags
    protected static final String MARKERS = "markers";
    protected static final String MARKER = "marker";

    protected String feedUrl;

    protected XMLParser(final String feedUrl) {
        this.feedUrl = feedUrl;
    }

    protected InputStream getInputStream() {
        try {
            return new URL(feedUrl).openConnection().getInputStream();
        } catch (IOException e) {
            Log.e("Routing Error", e.getMessage());
            return null;
        }
    }
}