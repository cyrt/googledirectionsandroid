package com.directions.route;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

/**
 * Created by cyril on 24/08/2016.
 */
public class Leg {
    private LatLng start_location;
    private LatLng end_location;
    private TextIntPair distance;
    private TextIntPair duration;

    private String start_address;
    private String end_address;

    private ArrayList<Step> steps;

    //private String[] traffic_speed_entry;
    private int[] via_waypoints;
    private PolylineOptions polyline;

    public LatLng getStartLocation() {
        return start_location;
    }

    public void setStartLocation(LatLng start_location) {
        this.start_location = start_location;
    }

    public LatLng getEndLocation() {
        return end_location;
    }

    public void setEndLocation(LatLng end_location) {
        this.end_location = end_location;
    }

    public TextIntPair getDistance() {
        return distance;
    }

    public void setDistance(TextIntPair distance) {
        this.distance = distance;
    }

    public TextIntPair getDuration() {
        return duration;
    }

    public void setDuration(TextIntPair duration) {
        this.duration = duration;
    }

    public String getStartAddress() {
        return start_address;
    }

    public void setStartAddress(String start_adress) {
        this.start_address = start_adress;
    }

    public String getEndAddress() {
        return end_address;
    }

    public void setEndAddress(String end_adress) {
        this.end_address = end_adress;
    }

    public ArrayList<Step> getSteps() {
        return steps;
    }

    public void setSteps(ArrayList<Step> steps) {
        this.steps = steps;
    }

    public int[] getViaWaypoints() {
        return via_waypoints;
    }

    public void setViaWaypoints(int[] via_waypoints) {
        this.via_waypoints = via_waypoints;
    }

    public PolylineOptions getPolyline() {
        if (null == polyline) {
            polyline = new PolylineOptions();
            for (Step s : steps) {
                polyline.addAll(s.getPolyline().getPoints());
            }
        }
        return polyline;
    }
}
